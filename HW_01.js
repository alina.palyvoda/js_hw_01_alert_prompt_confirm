/**
 * Created by Алина on 15.01.2022.
 */

// Теоретические вопросы
// 1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
    // Область видимости переменной var ограничивается функцией,
    // к этой переменной можно обратиться до ее объявления и при этом получить результат undefined.
    // Область видимости переменной let ограничивается не только функцией, но и любым другим блоком; при
    //обращении к переменной до ее объявления в результате получим ошибку.
    //Значение переменных var и let можно изменить, а значение переменной const - нет.
// 2. Почему объявлять переменную через var считается плохим тоном?
    // Область видимости переменной var оказывается очень большой, это может привести к непреднамеренной
    // перезаписи переменной и внести путаницу в код. Лучше, когда переменная имеет ограниченную область
    //видимости, как у переменной let, это сделает код более стабильным.


const allowedAge = 18;
const maxAge = 119;
const minAge = 5;
const secondAgeCriterion = 22;
let userName = prompt("Enter your name");
while (!userName) {
    userName = prompt("Enter your name");
}
let userAge = +prompt("Enter your age");
while (!userAge || userAge < minAge || userAge > maxAge) {
    userAge = +prompt("Incorrect value! Enter your age again");
}
if (userAge < allowedAge) {
    alert("You are not allowed to visit this website");
} else if (userAge >= allowedAge && userAge <= secondAgeCriterion) {
    if (confirm("Are you sure you want to continue?")) {
        alert(`Welcome, ${userName}`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`Welcome, ${userName}`);
}

